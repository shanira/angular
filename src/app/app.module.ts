import { HttpModule } from '@angular/http';
import { MessagesService } from './messages/messages.service';
import { UsersService } from './users/users.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { UsersComponent } from './users/users.component';

@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    UsersComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
      providers: [
        MessagesService,
        UsersService
       ],
  bootstrap: [AppComponent]
})
export class AppModule { }
