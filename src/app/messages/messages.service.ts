import { Injectable } from '@angular/core';
import {Http} from '@angular/http';

@Injectable()
export class MessagesService {
  http:Http;
  getMessages(){
    //return ['a','b','c'];
    //get messages from the SLIM rest API (Don't say DB)
    return  this.http.get('http://localhost/31023/lesson/slim/messages');
  }
  constructor(http:Http) { 
    this.http = http;
  }
}