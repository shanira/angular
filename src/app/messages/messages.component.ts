import { Component, OnInit } from '@angular/core';
import { MessagesService } from './messages.service';

@Component({
  selector: 'messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  
  //messages = ['a','b','c'];
  messages;
  messagesKeys;

  constructor(service:MessagesService) {
    //let service = new MessagesService();
    service.getMessages().subscribe(response=>{
      //console.log(response.json());
      this.messages = response.json();
      this.messagesKeys = Object.keys(this.messages);
    });
   }

  ngOnInit() {
  }

}
