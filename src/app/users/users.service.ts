import { Injectable } from '@angular/core';
import {Http} from '@angular/http';

@Injectable()
export class UsersService {
  http:Http;
  getUsers(){
    //get users from the SLIM rest API (Don't say DB)
    return  this.http.get('http://localhost/31023/lesson/slim/users');
  }
  constructor(http:Http) { 
    this.http = http;
  }
}